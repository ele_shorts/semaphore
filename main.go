package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	s := make(chan any, 3)
	var wg sync.WaitGroup

	for i := range "1234567890" {
		wg.Add(1)

		go func(w *sync.WaitGroup, i int) {
			defer w.Done()

			s <- true

			time.Sleep(2 * time.Second)
			fmt.Println("index: ", i, " -- semaphore -- ")

			<-s
		}(&wg, i+1)
	}
	wg.Wait()
}
